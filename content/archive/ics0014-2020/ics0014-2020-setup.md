+++
title = "ICS0014 2020 Setup"
description = ""
weight = 2
+++

## ICS0014 2020 Setup

##### Table of Contents  
**[Introduction](#introduction)**  
**[ICS0014](#ics0014)**  

### Introduction
This repository (setup-guides) acts as a collection for all my guides.  
ICS0014 does **not** require all the guides.

### ICS0014
1) [Backend](../../back-end)
Depending on your operation (OP) system the installation will be different
If you have problems ask help on slack or in one of the setup chats.
1) [git multiple remotes](../../git)
Guide for setting up class git.

Gradle is a used as a wrapper and should work out of the box 

Once you have cloned the project open it in IntelliJ.  
Make sure you open the folder of the project. build.gradle should be in the root of the project.
Right click on build.gradle and select "import as gradle project", if it says something other related to gradle it probably imported it automatically.
