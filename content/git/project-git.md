+++
title = "Project git"
description = ""
weight = 20
+++


For a team project it makes sense to share code between different users. Giving users appropriate rights. 
Compared to [class git](../class-git) project git is simple.

Create team repository in 3 simple steps
1) One of team members will need to create a private repository for your team using TalTech gitlab.
1) Add your team mates as maintainers or owners
1) Add lecturer and mentors as reporters 
