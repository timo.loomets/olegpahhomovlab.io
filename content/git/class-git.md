+++
title = "Classroom git"
description = ""
weight = 10
+++

Guide to setup git for **classroom coding**. 1 repository, 2 remotes.  
Don't use this for [project](project-git) as this is unnecessarily complex for that purpose.  

### Prerequisites
1) git

### Why 
1) you get teachers code
1) you get teachers code updates
1) you get to push and keep your own code

### Visual
<!-- ../ is needed because it renders it as subfolder -->
![](../class-git.png)

### Guide
1) Guide is done once per repository
1) Go to teacher's repository and click **fork**
    1) This creates you a copy of teacher's repository
1) Clone it.
    1) Part 1 done, you got the code. 
    1) However, soon you will realise that it is not up to date. 
    1) Rest of the guide setups the up-to-dateness
1) Open cloned project in IntelliJ
1) Open terminal (IntelliJ, git bash or cmd) in project root 
1) Add lecturer repository as remote named upstream
    ```bash
    git remote add upstream <teacher-repository-url>
    ```
    So for example:
    1) ics0014-2020-java-introduction
    
        {{< alert style="warning" >}} don't copy this 1:1 {{< /alert >}}
        ```bash
        git remote add upstream https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction
        ```
    1) if you have ssh setup you can replace https with git command
1) Check that you have 4 remotes (2 fetch, 2 push)
    ```bash
    git remote -v
    ```
1) Fetch upstream to check it works: 
    ```
    git fetch upstream
    ```
    If it doesn't - you probably made a typo. Remove using command below and add again. 
    ```
    git remote rm upstream
    ```

### Working with 2 repositories
1) All upstream/* are lecturer branches
1) All origin/* are your own pushed branches
1) You can pull from and push to origin
1) You can pull from but can't push to upstream
1) It makes sense to have same names for branches locally and remotely. If you are on master, reset upstream/master.
1) You have 3 options how to get code from upstream to your code.
    * merge (mix one with another)
    * reset (clear everything, loose local changes)
    * rebase (push upstream commits inside your commits)
1) merge, for example master
    ```bash
    git merge upstream/master
    ```
1) reset your branch, for example master 
    ```bash
    git reset --hard upstream/master
    ```
1) rebase, for example master
    ```bash
    git rebase upstream/master
    ```
1) if you get something like this:
CONFLICT (content): Merge conflict in src/main/java/ee/taltech/itcolledge/FirstContact.java
Automatic merge failed; fix conflicts and then commit the result.  
**Then go VCS (up top menu) -> git -> resolve conflicts**
1) merge, rebase and reset commands can be used from Intellij.  
Merge and rebase are accessible by right clicking in branches menu. 
Reset is a bit more complex, under log (alt + 9) you see commit history, 
there you can click reset current branch to here and pick hard option
