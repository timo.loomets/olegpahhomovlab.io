+++
title = "ICS0024 Mentors"
description = ""
weight = 100
+++


## Lecturer

{{< table style="table-striped" >}}
| name          | gitlab user   | role  |
| ------------- |-------------| -----|
| Oleg Pahhomov | olpahh        | lecturer |
{{< /table >}}

## ssh keys
It is easier just to add all keys

{{< code lang="bash" >}} 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1nHGXhRSlCggrgjPVFpXlRMy7D0P9OjzEr2yJ9QnCBdF/6N+rRSO96CJk/75wpmGIEjAq/pDVnaAd3zZBQ2F7b+1+ZFShQibrH6sL/v9M53oPGIMmWf0br3egwjqdXuvPVJ34QHfoa2hqJZDTScZMXJv+Z94dbLov0BCyo6ug4xqUaXU0sZroKkI8grTPBzVI7MaGOfbGM3fFLQsQsAgzEyWfHfrlLDIP/y6Y9ZdUEc6MRJWjPpeD7GogvrOfoG6YUxOzy9QrKB8fRF3oZp64xWScJ8l6wtrgHcihvyp1PJSq4VUbmMGozPOJUKwsMidjjjWmIvqwmfN2izkCwTX/ oleg@oleg-t480s
{{< /code >}}
