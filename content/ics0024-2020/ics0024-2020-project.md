+++
title = "ICS0024 Project"
description = ""
weight = 1
+++

## Introduction
Course is done in teams of 2-4 people. You **must** have a team. Solo teams are not allowed. There is much to gain by working together.  

To pass you need to complete 1 project which is graded in 3 parts:
1) Tested Modern Application 
1) Selenium and CypressJs
1) Testing Automation, CI/CD pipeline

### Registering a team
1) Register in excel 
    1) registration opens on official school year start (I need final list of participants) 
    1) link will be here
1) Create a **private** channel with name team_number_teamname where number is your team number, 
teamname is your team name (team name is optional)
    1) For example: team_14_happyoranges
    1) Important: teams < 10 use 0 before your number, so team_03_funnyarchitects
        1) It helps with slack order (for us)
    1) Create channel in iti0203-2020 slack, no need for separate slack
1) Invite all your team members and all [mentors](../mentors) to that channel

### Topic
topic is used for parts 1 and 3
todo update
1) Pick a topic [class topics]
1) Contact me in team chat what you've chosen. 
    1) One topic per team. 
    1) In the following lectures I want to combine all your calculators to our class project, add production, pipeline and front-end.
1) If there are not enough topics, I will create more. 

## Part 1 "Tested modern application"

### Goal
Build a modern backend API and test it with different tools.

### What are we doing
todo update
1) Each team is building a calculator
    * Calculator is something that takes in numbers, does some calculation and returns some result.
1) Calculator is structured as backend REST API
    * You can use class code as an example of API (delete everything unused)
    * You need 1 public method GET or POST /calculate
    * All of your calculation should be done with 1 request
1) As this is testing course, most of the points are given for tests.
    * Another big areas are 1) calculator API and 2) defense 
    * For full disribution [grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing) 
1) The future - after project is done, we will add all calculators to our class project. To simplify the process here are some things to think about:
    * All calculators take in a list of numbers.
    * Some calculators return no result, some return a single result and others return many results. 
    * User could choose which calculation they want

### Deadlines
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | *will be updated expect W5-W7* | extra points |
| on time | *will be updated expect W5-W7* | each hour late is -2% rounded upwards |
| late | *will be updated expect W5-W7* | 1 week late -30% |
| very late | *will be updated expect W5-W7* | more than 1 week late -100% |
{{< /table >}}

### Submitting
* Team must have a team chat with all of the team members and [mentors](../mentors)
    * We use team chat for communication and help. Contact directly only on personal matters.
    * Using team chats enables us to identify and tackle common problems.
* Teams must use use taltech gitlab (gitlab.cs.ttu.ee)
    * Add lecturer and all mentors to your gitlab project **as reporter**
        * usernames are in [mentors](../mentors)
    * No access to repository or analysis sheet is treated the same as no submission
* Team must confirm with the lecturers that they can access the code and analysis sheet
    * As there are many teams we are not able to connect email invitations with teams. 
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Here is our project repository <link to repository>. We have given you reporter access. 
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are so many and we have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)


### Grading
* todo update
* [The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)
* Wow, the grading excel is complex!? 
    * Yeah, we are trying to expand your horizon
    * There is git, API standards, business logic, spring, front-end.
* Deduction + extras system
    * Deduction is designed to reduce max points from 100% to 90% (5 -> 4)
    * These points can be regained by doing different extras
    * Extras give you opportunity to explore different topics
* We will grade main/master branch only 
    * [master -> main](../../git/main)
* Each team gets individual feedback
    * points + comments
* Small & beautiful is better than big and messy

### Defence
* Defences for part 1 are happening on:
    * will be updated ?expect W5-W7?
    * If none of the above work, let us know (in advance)
* All team members must attend the defence 
* Defence doodle is sent out after deadline to the teams that **have submitted**.
* Defence lasts 15 min
    * Have your backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1-2 min presentation of your application. (Not the code)
    * 4 min for questions from lecturer (theory)
    * 4 min for questions to lecturer (including grades)  
    * Rest 5 minutes are for buffers, small talk, for walking in and out etc.

        
## Part 2 "Selenium and CypressJs"

### Goal
Learn how testers write UI tests.

### What are we doing
* Create 2 new repositories, 1 for selenium and 1 for cypressJs
* Pick any public website and create 4 10-step tests
    * Same tests for both frameworks

### Deadlines
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | *will be updated expect W9-W11* | extra points |
| on time | *will be updated expect W9-W11* | each hour late is -2% rounded upwards |
| late | *will be updated expect W9-W11* | 1 week late -30% |
| very late | *will be updated expect W9-W11* | more than 1 week late -100% |
{{< /table >}}

### Submitting
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Here is our selenium repository <link to repository>. We have given you reporter access. 
    Here is our cypressJs repository <link to repository>. We have given you reporter access.
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are many teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
* [The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)
* Each team gets individual feedback
    * points + comments

### Defence
* There is no defence

## Part 3 "Production pipeline"
Continue working with your team and with your code from part 1.

### Goal
1) Commit to main/master branch triggers gitlab CI/CD pipeline. Code is compiled, tested, deployed to the server, application is restarted.  

### What are we doing
* Your team needs an empty virtual server. Pick one: Etais (TalTech machines), AWS, Azure, Digitalocean etc.
    * goodies: https://education.github.com/pack
    * for Etais send @OlegPahhomov your public ssh key
* You need to setup gitlab CI/CD setup for backend and frontend (optional) repositories.
* You need to setup production
    * Backend is running on the server as a linux service
    * Frontend application served by web server (Nginx/Apache) (optional)
    * Backend is proxied by web server (optional)
    * Or use some combination of docker to replace one (or all) of the above (optional)
* You need to create a detailed guide about all of the above 
* For more details: [grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)

### Deadlines
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | *will be updated expect W15* | extra points |
| on time | *will be updated expect W15* | each hour late is -2% rounded upwards |
| late | *will be updated expect W15* | 1 week late -30% |
| very late | *will be updated expect W15* | more than 1 week late -100% |
{{< /table >}}

### Submitting
* For grading we will commit to backend and frontend repositories expecting front-end and back-end production to change 
    * For that to work update us from **reporter** to **maintainer** 
    * Please confirm that we can push
* We also need access to your production server to validate your configuration
    * [mentors](../mentors) has our ssh keys
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Our application runs <link or ip>.
    To connect to our server use <ip>. We have added your ssh keys to the server.
    Here is our repository <link to repository>. We have given you maintainer access. 
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are many teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
* [The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)
* We will commit to backend and frontend repositories expecting front-end and back-end production to change 
* Each team gets individual feedback
    * points + comments

### Defence
* Defences for part 3 are happening on:
    * will be updated ?expect end of the course?
    * If none of the above work, let us know (in advance)
* All team members must attend the defence 
* Defence doodle is sent out after deadline to the teams that **have submitted**
* Defence lasts 15 min
    * Have your production, backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1-2 min presentation of your application. (Not the code)
    * 4 min for questions from lecturer (theory)
    * 4 min for questions to lecturer (including grades)  
    * Rest 5 minutes are for buffers, small talk, for walking in and out etc.

### Tips
* Start early
* Keep your server installation guide up to date!
* Add our [ssh keys](../mentors) to your server so we can help you

## Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2 + p3) / (p1max + p2max + p3max) * 100  
```  

{{< table style="table-striped" >}}
| percentage      | grade        | description  |
| ------------- |:---------------:| -----|
| 91-100 | 5 | excellent |
| 81-90  | 4 | very good |
| 71-80  | 3 | good |
| 61-70  | 2 | satisfactory |
| 51-60  | 1 | sufficient |
{{< /table >}}

[The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)
