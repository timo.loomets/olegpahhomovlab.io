+++
title = "Mac back-end"
description = ""
weight = 1
+++

1) *Git*
Install brew (https://brew.sh/)
{{< code lang="bash" >}}
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
{{< /code >}}
    Git will be installed automatically with brew
1) *Install JDK 11* 11 is LTE (long term support), don't install 12/13 etc
{{< code lang="bash" >}}
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk11
{{< /code >}}
1) *Install IntelliJ Ultimate*
    1) Download it from https://www.jetbrains.com/idea/download
    1) Register with TalTech email and you will get free student licence.
    1) To login in IntelliJ help -> register
    1) Install [plugins](../intellij-plugins)
