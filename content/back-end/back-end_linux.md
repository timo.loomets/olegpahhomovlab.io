+++
title = "Linux back-end"
description = ""
weight = 1
+++

1) *Install git*  
{{< code lang="bash" >}}
sudo apt-get install git
{{< /code >}}
    apt or apt-get depends on your distribution
1) *Install OpenJDK 11*  11 is LTE (long term support), don't install 12/13 etc
{{< code lang="bash" >}}
sudo apt-get install openjdk-11-jdk  
{{< /code >}}
1) *Install IntelliJ Ultimate*
    1) Download it from https://www.jetbrains.com/idea/download
    1) Register with TalTech email and you will get free student licence.
    1) To login in IntelliJ help -> register
    1) Install [plugins](../intellij-plugins)
