+++
title = "Windows back-end"
description = ""
weight = 1
+++

## Back-end setup guide for windows

1) *Git*
    1) Download it: https://git-scm.com/download/win
    1) Install it
2) *Install Oracle JDK 11*
    1) Download it (must create account first) https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html
    1) Install it
1) *Install IntelliJ Ultimate*
    1) Download it from https://www.jetbrains.com/idea/download
    1) Register with TalTech email and you will get free student licence.
    1) To login in IntelliJ help -> register
    1) Install [plugins](../intellij-plugins)

