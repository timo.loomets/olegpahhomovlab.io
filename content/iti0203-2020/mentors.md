+++
title = "ITI0203 Mentors"
description = ""
weight = 100
+++


## Mentors

{{< table style="table-striped" >}}
| name          | gitlab user   | role  | angular | react | vue |  aws | azure
| ------------- |-------------  | ----- | ----- | ----- | ----- | ----- | ----- |
| Oleg Pahhomov | olpahh        | lecturer | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Ilja Samoilov | ilsamo        | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Tavo Annus    | taannu        | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
| Marten Jürgenson    | mjurge  | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
| Oskar Pihlak  | ospihl        | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Enrico Vompa   | envomp        | mentor   | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Timo Loomets   |  coming soon       | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
{{< /table >}}

## ssh keys
It is easier just to add all keys

{{< code lang="bash" >}} 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1nHGXhRSlCggrgjPVFpXlRMy7D0P9OjzEr2yJ9QnCBdF/6N+rRSO96CJk/75wpmGIEjAq/pDVnaAd3zZBQ2F7b+1+ZFShQibrH6sL/v9M53oPGIMmWf0br3egwjqdXuvPVJ34QHfoa2hqJZDTScZMXJv+Z94dbLov0BCyo6ug4xqUaXU0sZroKkI8grTPBzVI7MaGOfbGM3fFLQsQsAgzEyWfHfrlLDIP/y6Y9ZdUEc6MRJWjPpeD7GogvrOfoG6YUxOzy9QrKB8fRF3oZp64xWScJ8l6wtrgHcihvyp1PJSq4VUbmMGozPOJUKwsMidjjjWmIvqwmfN2izkCwTX/ oleg@oleg-t480s
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC1BM42BRG301eKNYfkROaIv7uMOn2EQnBrtrFBB6whrxqkE1qlWRiQC75++r9Xaueg+ZxqSlWuV88+hTE4A3n8QD10+nCJgjv6OH38Mmbv0uod83G4+qHQuZCz9vlbPBZ1lqfFCt0fvBr45Nc4vSTiDK7lZ977TJrVwjO57MsKJYF1+Uyl28bXZad+byAODVknHUNHSK/nMKNnrarogIivbk7Xu3ADu/W3TG3O7iFTIm5lGRYkOcE6grVcKPPxonpKm9LoUEu5UbABbM6+I1vJ+sozaE4duM3u05cC1ePp1gQx312ub01GILVl92ZtqDrd9VxAdA27aJdcksZr5f8MGlBqrVc11I0uSQgeEbnlq3C5q4VnIDo0xfvCrgcHtjutag+Za+5lfQc5oRaKs/olbm4skLtxcZ3thGP7E6HqrBIsWyZ7bf7sNrKephjGnr216IAQkGneTDO+MSo4JPLP8wf8D2pE11RtKUEgZh+MniKR9LRGInVJGYOx0uIaI4k= tavo@tavo-Z97-HD3
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDR2/yOvmsfabcLu7ny8/ZMHmIGzA1DH9PwhVwXSofDpjahRvDnaE8kHgJkCsnCerZZ2qhZCY2JTIIVzillEqvwMfRNix7huQJ5JG4BKw/Ph75e1atOBFZlkRkqn66nrMsAGMxcdAbSYmwDjO/FSjD+WGkQgOuHzrIyZScmdiy7sNxoCBDdLR09NqJ6+oRKlhfXazsWWByTs4h5dgHaZbp52mEXqibsv0FHGqJIh1ks312xnQ3n80t+tA5cJz75r/jTWS/d8sKRXrCsIcwNMdmTDoexDcURKeP6Kr3LUQWyRNGRlVK2cwE+iX1qbuwv78UWNmILTk7NJ6tUh+1An2ivy8wm5MXxtOs9guSOp5S0DHsXIurJrg/TArq0MHuElSUYjekN+ugFozRjKNPLjPLpqxbikBLoEz0laOJ3c3R5GIKgTT+uEaA2ieEhtnMkg+t7HH9qRS/Kv/LBO0cia5pHIJ4iXj809OpMdPtwOfZDeuXweJK5b6ecDnifsbT4iJbhxKl5Ly7YTfuxrw9Ynf6XZxR3aJQ9g1xnYJfnJML4gwQMWGBJoSMY1a/WfHh7wdvFHqSd8kC8Dta8QyEnwU6tMxnSEfQ52DoJgViwV75zGEHJkRMIZD8oE1ZjQARKIYLGesdKXo4ZQdS45Vc0sTh4equV2zH1dOUH+7b2OltLJQ== isamoilov@twilio.com
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAsrxCNCDJ1eGbywdXNElHa6GQX5wTbxsZYxY0OIXosGPR/fDDki9Cn9e8BZEJkvXpBRJ9Pv6+eCTDH1/o2FAH9L02OTZGSFuJo7b2s48ImMa5lUkCMIhsgW3yWCVbwA0T/MoTnahimaywpmHkiK6yFpr/wSteKV7n1G6bh7AdJDBzK02p+6aL46QCeTknO/bZjEc7foalam9Yo8VoGYBoqKKrhaGCm5aNElvzaWDMY4yN47NIF043YSM41O6T7lsuOSjB6cWK1qZ0RNV12r4fpkdeOPfBgGx8aOSfavcM6skhdZd3aH8AekmUxKKwdt/+Ezay3ckbTgh4C4CfyktIuQ== rsa-key-20200816
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDT+b1hnf7qYVwD7gnZbsBAdIK0tK79ngyhVCdvbs4ngTJz/0jYBLzOO9FOAu78HSB/pEvKRWlqI9P/PeHxm3Xjht3XRsj+7B83EF33+L4sH93nJ47Vi29kcNxAN+1jW0RArLwb15RDwbF6n+/wXc+BVDKgNYdw4vXnFgoU1RvvdwNMk9LQi5xHye9fnWlL/vaQUJRYo/ryk/tUPCNMAC+9K5yLUYk0ziOZgF9g0gcdDASvY1Im9EYbCOqxdzvi5+kuW2aZyv0K0t1Sa0VTjmNTHWXgVmdrLMhfiZ+3sUArxFgTVsCZWdjfAXMSHdG612voyxNg9rhDalae2bJ5dHtn webmedia\oskarp@laptop1480
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9zn9MkR5Bw3bgIPbW5bPIGe1l380U41rSDEwmF++NEWayQsWzDtOLvz7JLTgwpsJUbeZgpQ+sqSu9lN41bnSPaxGlBfk7mcVK0/9wk4sKRQUQgIFFctLOw6kVobPg8tHcdKc93LFHlkkfR/1nkxwaNNX8MoCWP23uc+6gDmdLD62lNcBHk0oEm19aWhCAGltqAROVJgWY8JFmXkShZbrvDrmM6NQGAyUry55scFZxfk4frDhyPzH/K1gH6f2BTDMYrNWhe3tdtGuJMoPS9pyAtAyFOtnNiw6seIbL70ny0OuYg03iR6bltfxf5S4M9HtP85c5AG+VQBscyvdVrH/p envomp@DESKTOP-4BQ0VLQ
{{< /code >}}
