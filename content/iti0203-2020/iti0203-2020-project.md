+++
title = "ITI0203 Project"
description = ""
weight = 2
+++



## Introduction
Course is done in teams of 2-4 people. You **must** have a team. Solo teams are not allowed. There is much to gain by working together.  


To pass you need to complete 1 project which is graded in 3 parts:
1) Simple modern application
1) Gitlab CI/CD, production
1) Securing with Spring, finalizing application



### Registering a team
1) Register in excel 
    1) registration opens on official school year start (I need final list of participants) 
    1) link will be here
1) Create a **private** channel with name team_number_teamname where number is your team number, 
teamname is your team name (team name is optional)
    1) For example: team_14_happyoranges
    1) Important: teams < 10 use 0 before your number, so team_03_funnyarchitects
        1) It helps with slack order (for us)
    1) Create channel in iti0203-2020 slack, no need for separate slack
1) Invite all your team members and all [mentors](../mentors) to that channel

### Topic
You will work on the same topic for the whole course.
Pick your own topic or use one of [lecturer topics](https://docs.google.com/document/d/1gRZVxAJq5MU_cqml38r1PgQtmgRdRmoDZfQF0bgCJMk/edit?usp=sharing)  
Here are some [APIs](https://rapidapi.com/) you can use.  
If you use an external API then it must be connected through your back-end not your front-end. Using external APIs is encouraged.

Building browser-focused games is not allowed, because we want to develop back first, front second. 

## Part 1 "Simple modern application"

Start working with your team and with your code.  

### Goal
Build a simple modern application.
Backend, frontend, git.

### What are we doing?
* Now it's your turn to create your (first?) modern application
* First you need to have a team, topic and some plan
    * The plan is what I call "analysis sheet"
* Then develop back-end API and some front-end
* [Grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing) provides some guidance as well

### Deadlines
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | *will be updated expect W5-W7* | extra points |
| on time | *will be updated expect W5-W7* | each hour late is -2% rounded upwards |
| late | *will be updated expect W5-W7* | 1 week late -30% |
| very late | *will be updated expect W5-W7* | more than 1 week late -100% |
{{< /table >}}

* In real world it is analogous to a production deadline. Marketing is ready, all demos and events are booked and IT team must deliver.
    * If you deliver early, life is good
* If you need an extension and have a valid reason(s), contact us. 
    * Extension is not given on the last day.
    * In 2019 some teams were involved with army training

### Submitting
* Team must have a team chat with all of the team members and [mentors](../mentors)
    * We use team chat for communication and help. Contact directly only on personal matters.
    * Using team chats enables us to identify and tackle common problems.
* Teams must use use taltech gitlab (gitlab.cs.ttu.ee)
    * Add lecturer and all mentors to your gitlab project **as reporter**
        * usernames are in [mentors](../mentors)
    * No access to repository or analysis sheet is treated the same as no submission
* Team must confirm with the lecturers that they can access the code and analysis sheet
    * As there are 30 teams we are not able to connect email invitations with teams. 
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Here is our back-end repository <link to repository>. We have given you reporter access. 
    Here is our front-end repository <link to repository>. We have given you reporter access. 
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
* [The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)
* Wow, the grading excel is complex!? 
    * Yeah, we are trying to expand your horizon
    * There is git, API standards, business logic, spring, front-end.
* Deduction + extras system
    * Deduction is designed to reduce max points from 100% to 90% (5 -> 4)
    * These points can be regained by doing different extras
    * Extras give you opportunity to explore different topics
* We will grade main/master branch only 
    * [master -> main](../../git/main)
* Each team gets individual feedback
    * points + comments
* Small & beautiful is better than big and messy
* Spring Security is not graded in part 1

### Analysis sheet
**todo update**  
Analysis sheet has multiple purposes. It is a tool for you and for us.
* It defines what you want to do
* It defines order of priorities.
* It is a tool for tracking where you are.
* It is a tool for dividing up responsibilities.
* It helps us grade. We grade you against what you planned to do. It is better to have something small and simple that works, then something big and complicated that doesn't.
* Analysis sheet will grow for part 2 and 3 of the project.
* You can ask feedback from us on your analysis sheet. We can tell you to be more specific, to have less stuff... etc, etc.  
* I recommend you use user stories + 1 user story for setting everything up.  
    * [User stories description](https://www.mountaingoatsoftware.com/agile/user-stories)  
    * [Class project user stories](https://docs.google.com/document/d/1sgqeR2vZ10mQ_UJe4qtD-tNOuQbvmCIaOXIzjB4n724/edit?usp=sharing)

### Defence
* Defences for part 1 are happening on:
    * will be updated ?expect W5-W7?
    * If none of the above work, let us know (in advance)
* All team members must attend the defence 
* Defence doodle is sent out after deadline to the teams that **have submitted**.
* Defence lasts 15 min
    * Have your backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1-2 min presentation of your application. (Not the code)
    * 4 min for questions from lecturer (theory)
    * 4 min for questions to lecturer (including grades)  
    * Rest 5 minutes are for buffers, small talk, for walking in and out etc.

## Part 2 "Gitlab CI/CD, production"

Continue working with your team and with your code.  

### Goal
Commit to main/master branch triggers gitlab CI/CD pipeline. 
Code is compiled, tested, deployed to the server, application is restarted.  

### What are we doing?
* Your team needs an empty virtual server. Pick one: AWS, Azure, Digitalocean etc.
    * goodies: https://education.github.com/pack
* You need to setup gitlab CI/CD setup for backend and frontend repositories.
* You need to setup production
    * Backend is running on the server as a linux service
    * Frontend application served by web server (Nginx/Apache)
    * Backend is proxied by web server
    * Or use some combination of docker to replace one (or all) of the above
* You need to create a detailed guide about all of the above 
* For more details: [grading excel part 2](https://docs.google.com/spreadsheets/d/19OaDYavBg7YLNa3cyCj5GVvd-Tk8lqWLi3-84_FU-yw/edit#gid=1231401474)
    * There are tons of extras!

### Deadlines
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | *will be updated expect W9-W10* | extra points |
| on time | *will be updated expect W9-W10* | each hour late is -2% rounded upwards |
| late | *will be updated expect W9-W10* | 1 week late -30% |
| very late | *will be updated expect W9-W10* | more than 1 week late -100% |
{{< /table >}}

### Submitting
* For grading we will commit to backend and frontend repositories expecting front-end and back-end production to change 
    * For that to work update us from **reporter** to **maintainer** 
    * Please confirm that we can push
* We also need access to your production server to validate your configuration
    * [mentors](../mentors) has our ssh keys
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Our application runs <link or ip>.
    To connect to our server use <ip>. We have added your ssh keys to the server.
    Here is our back-end repository <link to repository>. We have given you maintainer access. 
    Here is our front-end repository <link to repository>. We have given you maintainer access.
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
* [The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)
* We will commit to backend and frontend repositories expecting front-end and back-end production to change 
* Each team gets individual feedback
    * points + comments

### Defence
* There is no defence
    * We think committing to your repository and seeing what happens is enough

### Tips
* Start early
* Keep your server installation guide up to date!
* Add our [ssh keys](../mentors) to your server so we can help you

## Project part 3 "Securing with Spring, finalizing application"

Continue working with your team and with your code

### Goal
Secure application with Spring Security. Finalize. Fix bugs. Improve design.

### What are we doing?
* We are working on our Application again.
* We are adding Spring Security for registration and roles (admin/user).
* We are fixing our bugs so application works as intended.
* We are adding angular-material so application looks nice.
* We are adding some daemon processes like synchronization because we can.

### Deadlines
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | *will be updated expect W15* | extra points |
| on time | *will be updated expect W15* | each hour late is -2% rounded upwards |
| late | *will be updated expect W15* | 1 week late -30% |
| very late | *will be updated expect W15* | more than 1 week late -100% |
{{< /table >}}

### Submitting
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Our application runs <link or ip>.
    To connect to our server use <ip>. We have added your ssh keys to the server.
    Here is our back-end repository <link to repository>. We have given you maintainer access. 
    Here is our front-end repository <link to repository>. We have given you maintainer access.
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)
* While it is not mandatory, it is recommended to submit together with production as it speeds up grading.

### Grading
* [The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)
* Structure and points are similar to Part 1. Following things are new.
    * We want you to rewrite analysis sheet to show what was done in part 1 and what will be done in part 3. You can include refactoring and bugs in the planned tasks.
    * User stories are graded for working well. Bug-free. As expected.
    * Spring Security is graded. User can register. Admin/user can access their respective resources.
    * Part 2 still works.
    * Your application looks good in mobile.

### Defence
* Defences for part 3 are happening on:
    * will be updated ?expect end of the course?
    * If none of the above work, let us know (in advance)
* All team members must attend the defence 
* Defence doodle is sent out after deadline to the teams that **have submitted**
* Defence lasts 15 min
    * Have your production, backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1-2 min presentation of your application. (Not the code)
    * 4 min for questions from lecturer (theory)
    * 4 min for questions to lecturer (including grades)  
    * Rest 5 minutes are for buffers, small talk, for walking in and out etc.

## Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2 + p3) / (p1max + p2max + p3max) * 100  
```  

{{< table style="table-striped" >}}
| percentage      | grade        | description  |
| ------------- |:---------------:| -----|
| 91-100 | 5 | excellent |
| 81-90  | 4 | very good |
| 71-80  | 3 | good |
| 61-70  | 2 | satisfactory |
| 51-60  | 1 | sufficient |
{{< /table >}}

[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)
